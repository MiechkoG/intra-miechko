
420-523 -- Examen Intra -- Automne 2016


Premières choses à faire :

- Faites un FORK de ce dépot en le renommant intra-<votre prénom>.

- Donnez accès à l'usager ngorse.

- Faites un git clone de ce dépot sur votre ordinateur.


Directives :

- Les questions se trouvent dans questions-reponses/.

- Répondez directement dans les fichiers prévus à cet effet.

- Placez votre code dans le répertoire prévu à cet effet (question C).

- Une fois votre examen terminé, assurez-vous que vous avez bien fait un commit de tous les fichiers nécessaires.


