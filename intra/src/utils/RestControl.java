package utils;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bean.User;
import bean.UserDB;

@RestController
public class RestControl {
	@RequestMapping(value = "/userinfo")
	public User userInfo(@RequestParam(value = "id") long id) {
		UserDB db = new UserDB();
		User user;
		try {
			user = db.getUserById(id);
		} catch (UserNotFoundException e) {
			user = new User(-1, "not found", "");
		}
		return user;
	}
}
