package bean;

import utils.UserNotFoundException;

public class UserDB {
	public User getUserById(long id) throws UserNotFoundException{
		if(id == 1){
			return new User(1, "Bob", "Bob@email.com");
		}
		else{
			throw new UserNotFoundException("l'usager n'existe pas");
		}
	}
}
